//https://stackoverflow.com/questions/17567624/pass-a-parameter-to-a-content-script-injected-using-chrome-tabs-executescript

document.addEventListener('DOMContentLoaded', function() {
	var myHeading 				=	document.getElementById('myHeading');	
	
	var firememDev 				= 	document.getElementById('firememDev');	
	var firememProd  			=	document.getElementById('firememProd');
	var firememPlat 			=	document.getElementById('firememPlat');
	var acvancePreference 		=	document.getElementById('acvancePreference');
	var acvancePreferenceLabel  =   document.getElementById('acvancePreferenceLabel');	
	
	
	var iavLabel				= 	document.getElementById('iavLabel');	
	var iavPlusLabel			= 	document.getElementById('iavPlusLabel');
	var nintyDaysTrans 			= 	document.getElementById('nintyDaysTrans');
	var fourtyFiveDaysTrans 	= 	document.getElementById('fourtyFiveDaysTrans');
	var IAVTicker 				= 	document.getElementById('IAVTicker');
	
  
  
   chrome.tabs.getSelected(null, function(tab) {
        var tablink = tab.url;
        var url = tablink.toString();
		
        if (url.search('dumpLink=')> -1) {			
			console.log("dumpLink is"+ url);	
			myHeading.innerText = "Firemem from this dump";
            nintyDaysTrans.className 			= 'hide';
			fourtyFiveDaysTrans.className		= 'hide';
			IAVTicker.className					= 'hide';
			iavLabel.className					= 'hide';
			iavPlusLabel.className				= 'hide';
			
			
			
        }else if(url.search('firemem')> -1){
			console.log("firemem is"+ url);
			myHeading.innerText = "Advance preferences";
            
			firememDev.className 				= 'hide';
			firememProd.className 				= 'hide';
			firememPlat.className				= 'hide';
			acvancePreferenceLabel.className 	= 'hide';
			
			
			
			
			
			
		}
		
    });
	
  IAVTicker.addEventListener('click', function() { 
 	  
	 
		chrome.tabs.query({active: true,currentWindow: true}, function(tabs) {            
			console.log("IAVTicker firememDev");	
			
			
			chrome.tabs.executeScript(tabs[0].id, {
						code: "var transDays = \"na\";",runAt:'document_start'
						},function() {
							chrome.tabs.executeScript(tabs[0].id, {file: 'IAVPlusTicker.js',runAt:'document_start'});
			});


			
			
		
	});
  }, false);
  
  fourtyFiveDaysTrans.addEventListener('click', function() { 
 	  
	 
		chrome.tabs.query({active: true,currentWindow: true}, function(tabs) {            
			console.log("nintyDaysTrans firememDev");	
			var transDays = '45' ;
			
			chrome.tabs.executeScript(tabs[0].id, {
						code: "var transDays = "+transDays+";",runAt:'document_start'
						},function() {
							chrome.tabs.executeScript(tabs[0].id, {file: 'IAVPlusTicker.js',runAt:'document_start'});
			});


			
			
		
	});
  }, false);
  
  
  nintyDaysTrans.addEventListener('click', function() { 
 	  
		chrome.tabs.query({active: true,currentWindow: true}, function(tabs) {            
			console.log("nintyDaysTrans firememDev");	
			var transDays = '90' ;
			
			chrome.tabs.executeScript(tabs[0].id, {
						code: "var transDays = "+transDays+";",runAt:'document_start'
						},function() {
							chrome.tabs.executeScript(tabs[0].id, {file: 'IAVPlusTicker.js',runAt:'document_start'});
			});
	});
  }, false);
  
  
  firememDev.addEventListener('click', function() { 
 	  
		chrome.tabs.query({active: true,currentWindow: true}, function(tabs) {            
			console.log("clicking firememDev");	
			var advPref = 'false' ;
			var firememEnv = 'firememDev';
			
			if (acvancePreference.checked){
				console.log("acvancePreference checked");
				advPref	 = 'true';
			}
			chrome.tabs.executeScript(tabs[0].id, {
						//code: "var advPref = "+advPref+";",runAt:'document_start'
						code: "var pref = {advPref:'"+advPref+"',firememEnv:'"+firememEnv+"'};",runAt:'document_start'
						},function() {
							chrome.tabs.executeScript(tabs[0].id, {file: 'IAVDumpAnalyser.js',runAt:'document_start'});
			});
	});
  }, false);
  
  firememProd.addEventListener('click', function() { 
 	  
		chrome.tabs.query({active: true,currentWindow: true}, function(tabs) {            
			console.log("clicking firememProd");	
			var advPref = 'false' ;
			var firememEnv = 'firememProd';
			if (acvancePreference.checked){
				console.log("acvancePreference checked");
				advPref	 = 'true';
			}
			chrome.tabs.executeScript(tabs[0].id, {
						code: "var pref = {advPref:'"+advPref+"',firememEnv:'"+firememEnv+"'};",runAt:'document_start'
						},function() {
							chrome.tabs.executeScript(tabs[0].id, {file: 'IAVDumpAnalyser.js',runAt:'document_start'});
			});
	});
  }, false);
  
  firememPlat.addEventListener('click', function() { 
 	  
		chrome.tabs.query({active: true,currentWindow: true}, function(tabs) {            
			console.log("clicking firememPlat");	
			var advPref = 'false' ;
			var firememEnv = "firememPlat";
			if (acvancePreference.checked){
				console.log("acvancePreference checked");
				advPref	 = 'true';
			}
			chrome.tabs.executeScript(tabs[0].id, {
						code: "var pref = {advPref:'"+advPref+"',firememEnv:'"+firememEnv+"'};",runAt:'document_start'
						},function() {
							chrome.tabs.executeScript(tabs[0].id, {file: 'IAVDumpAnalyser.js',runAt:'document_start'});
			});
	});
  }, false);
  
  
  
  
  
  
  
}, false);

